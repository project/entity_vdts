<?php

declare(strict_types=1);

namespace Drupal\Tests\entity_vdts\Functional;

use Drupal\Core\Entity\Entity\EntityViewDisplay;
use Drupal\Tests\BrowserTestBase;
use Drupal\entity_vdts\EntityViewDisplayTemplateSuggestionsInterface;
use Drupal\node\NodeInterface;

/**
 * Test template suggestions.
 *
 * @group entity_vdts
 */
class TemplateSuggestionsTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'entity_vdts',
    'node',
  ];

  /**
   * A test node.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected NodeInterface $node;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Create a content type.
    $this->drupalCreateContentType(
      [
        'type' => 'page',
        'name' => 'Basic page',
      ]
    );

    // Create a node.
    $this->node = $this->createNode();
  }

  /**
   * Test the bare suggestion.
   */
  public function testBareSuggestion(): void {
    $this->enableTwigDebugMode();

    $this->drupalGet($this->node->toUrl());
    $presents = [
      'node.html.twig',
    ];
    $absents = [
      'node--bare.html.twig',
    ];
    $this->checkSuggestions($presents, $absents);

    // Enable bare suggestion.
    $view_display = EntityViewDisplay::load('node.page.default');
    if ($view_display == NULL) {
      $this->fail('Impossible to load the view display.');
    }
    $view_display
      ->setThirdPartySetting(EntityViewDisplayTemplateSuggestionsInterface::CONFIG_KEY, 'bare', TRUE)
      ->save();

    $this->drupalGet($this->node->toUrl());
    $presents = [
      'node.html.twig',
      'node--bare.html.twig',
    ];
    $absents = [];
    $this->checkSuggestions($presents, $absents);
  }

  /**
   * Check suggestions.
   *
   * @param array $presents
   *   The suggestions that should be present.
   * @param array $absents
   *   The suggestions that should be absent.
   */
  protected function checkSuggestions(array $presents, array $absents): void {
    foreach ($presents as $present) {
      $this->assertSession()->responseContains($present);
    }

    foreach ($absents as $absent) {
      $this->assertSession()->responseNotContains($absent);
    }
  }

  /**
   * Enable Twig debug mode.
   */
  protected function enableTwigDebugMode(): void {
    // Enable debug, rebuild the service container, and clear all caches.
    /** @var array $parameters */
    $parameters = $this->container->getParameter('twig.config');
    $parameters['debug'] = TRUE;
    // Ignore until https://www.drupal.org/project/drupal/issues/3414535 is
    // fixed.
    // @phpstan-ignore-next-line
    $this->setContainerParameter('twig.config', $parameters);
    $this->rebuildContainer();
    $this->resetAll();
  }

}
