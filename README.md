# Entity View Display Template Suggestions

This module aims to allow entity templates to get suggestions from the entity
view display configuration.

The goal is to allow to have generic templates not related to a view mode and
to be able to configure if the view mode should use it or not.

Currently, the module only allows to have a bare template suggestion.


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

After enabling the module, go on a view display configuration like
`admin/structure/types/manage/page/display` and you will see a new
configuration option.
