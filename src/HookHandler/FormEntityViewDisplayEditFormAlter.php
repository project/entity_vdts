<?php

declare(strict_types=1);

namespace Drupal\entity_vdts\HookHandler;

use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\entity_vdts\EntityViewDisplayTemplateSuggestionsInterface;
use Drupal\field_ui\Form\EntityDisplayFormBase;

/**
 * Alter entity view display form.
 */
class FormEntityViewDisplayEditFormAlter {

  use DependencySerializationTrait;
  use StringTranslationTrait;

  /**
   * Alter entity view display form.
   */
  public function alter(array &$form, FormStateInterface $form_state): void {
    $form_object = $form_state->getFormObject();
    if (!($form_object instanceof EntityDisplayFormBase)) {
      return;
    }

    $display = $form_object->getEntity();
    if (!($display instanceof EntityViewDisplayInterface)) {
      return;
    }

    $form[EntityViewDisplayTemplateSuggestionsInterface::CONFIG_KEY] = [
      '#type' => 'details',
      '#title' => $this->t('Template suggestions'),
      '#description' => $this->t('This allows to configure the addition of template suggestions, then it is the responsibility of the theme to provide such templates.'),
      '#tree' => TRUE,
    ];

    $form[EntityViewDisplayTemplateSuggestionsInterface::CONFIG_KEY]['bare'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Add "bare" template suggestion'),
      '#description' => $this->t('Add a template suggestion %suggestion.', [
        '%suggestion' => 'ORIGINAL_THEME_HOOK--bare.html.twig',
      ]),
      '#default_value' => $display->getThirdPartySetting(EntityViewDisplayTemplateSuggestionsInterface::CONFIG_KEY, 'bare'),
    ];

    $form['#entity_builders'][] = [$this, 'entityBuilder'];
  }

  /**
   * Save configuration.
   */
  public function entityBuilder(string $entity_type_id, EntityViewDisplayInterface $display, array &$form, FormStateInterface &$form_state): void {
    $values = $form_state->getValue(EntityViewDisplayTemplateSuggestionsInterface::CONFIG_KEY);
    if (!\is_array($values)) {
      return;
    }

    foreach ($values as $key => $value) {
      if ($key == 'bare' && !$value) {
        $display->unsetThirdPartySetting(EntityViewDisplayTemplateSuggestionsInterface::CONFIG_KEY, $key);
      }
      else {
        $display->setThirdPartySetting(EntityViewDisplayTemplateSuggestionsInterface::CONFIG_KEY, $key, $value);
      }
    }
  }

}
