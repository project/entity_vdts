<?php

declare(strict_types=1);

namespace Drupal\entity_vdts\HookHandler;

use Drupal\entity_vdts\EntityViewDisplayTemplateSuggestionsInterface;

/**
 * Add template suggestions.
 */
class ThemeSuggestionsAlter {

  /**
   * Add template suggestions.
   *
   * @param array $suggestions
   *   The list of suggestions.
   * @param array $variables
   *   The theme variables.
   * @param string $hook
   *   The theme hook.
   */
  public function alter(array &$suggestions, array $variables, string $hook): void {
    $key = '#' . EntityViewDisplayTemplateSuggestionsInterface::CONFIG_KEY;
    if (!isset($variables['elements'][$key]) || empty($variables['elements'][$key])) {
      return;
    }

    // Currently only bare is provided.
    if (isset($variables['elements'][$key]['bare']) && $variables['elements'][$key]['bare']) {
      if (isset($variables['theme_hook_original'])) {
        $suggestions[] = $variables['theme_hook_original'] . '__bare';
      }
    }
  }

}
