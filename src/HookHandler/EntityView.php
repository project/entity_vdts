<?php

declare(strict_types=1);

namespace Drupal\entity_vdts\HookHandler;

use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\entity_vdts\EntityViewDisplayTemplateSuggestionsInterface;

/**
 * Transfer data from entity view display to build array.
 */
class EntityView {

  /**
   * Transfer data from entity view display to build array.
   *
   * @param array &$build
   *   A renderable array representing the entity content. The module may add
   *   elements to $build prior to rendering. The structure of $build is a
   *   renderable array as expected by
   *   \Drupal\Core\Render\RendererInterface::render().
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity object.
   * @param \Drupal\Core\Entity\Display\EntityViewDisplayInterface $display
   *   The entity view display holding the display options configured for the
   *   entity components.
   * @param string $view_mode
   *   The view mode the entity is rendered in.
   */
  public function alter(array &$build, EntityInterface $entity, EntityViewDisplayInterface $display, string $view_mode): void {
    $build['#' . EntityViewDisplayTemplateSuggestionsInterface::CONFIG_KEY] = $display->getThirdPartySettings(EntityViewDisplayTemplateSuggestionsInterface::CONFIG_KEY);
  }

}
