<?php

declare(strict_types=1);

namespace Drupal\entity_vdts;

/**
 * Provides an interface for entity_vdts constants.
 */
interface EntityViewDisplayTemplateSuggestionsInterface {

  /**
   * The config key of the module.
   */
  public const CONFIG_KEY = 'entity_vdts';

}
